import datetime
import json
import csv
import requests
from django.db import connection
from  yelpscraper.models  import Search, Result, ApiData, Status
from requests_html import HTMLSession
import time
import re
import os
import django


with HTMLSession() as session: 
    
  results = Result.objects.filter(uid=uid)
  
  for result in results:
      
    try:
      r = session.get(result.yurl)
      time.sleep(20)
      url = r.html.find('.biz-website.js-biz-website.js-add-url-tagging a', first=True)
      print(url.text)
      result.surl = url.text
      result.save()
      
    except Exception as e:
      print(e)
      
