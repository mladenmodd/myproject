from django import forms
from django.contrib.auth.forms import AuthenticationForm
import logging
"""last commit: 08012019 12:43 am"""


class SearchForm(forms.Form):
    location = forms.CharField(label='Enter Location', max_length=200, widget=forms.TextInput(attrs={'class' : 'input100'}))
    term = forms.CharField(label='Enter Term', max_length=200, widget=forms.TextInput(attrs={'class' : 'input100'}))

