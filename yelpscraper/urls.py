"""views.py: views for website"""
"""last commit: 08012019 12:43 am"""

from django.urls import path, include
from . import views
import logging
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView



logger = logging.getLogger(__name__)


urlpatterns = [
    path('', views.index, name='index'),
    path('history', views.history, name='history'),
    path('login', LoginView.as_view(), name='login'),
    path('search',  views.search.as_view(), name='search'),
    path('download/<str:uid>', views.dwn_csv, name='download'),
    path('table/<str:uid>', views.table, name='table'),
    path('deepsearchurl/<str:uid>', views.deep_search_url, name='deepsearch_url'),
    path('deepsearchmail/<str:uid>', views.deep_search_mail, name='deepsearch_mail'),
    path('status', views.status, name='status'),
    path('upload_csv', views.upload_csv, name='upload_csv'),
    path('master', views.master, name='master'),
    path('states/<str:state>', views.tablestate, name='tablestate'),
    path('term/<str:term>', views.tableterm, name='tableterm'),
    path('states', views.state, name='states'),
    path('terms', views.term, name='terms'),
    path('cities', views.cities, name='cities'),
    path('cities/<str:state>/<str:city>', views.city, name='city'),
    path('ajax/check_status', views.check_status, name='check_status'),
]
