"""views.py: views for website"""
"""last commit: 08012019 02:08 am"""


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
from django.contrib.auth import authenticate
from django.contrib.auth.views import LoginView
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from yelpscraper.models import Search, Result, ApiData, State, Status
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.urls import reverse
from django.template import loader
from . import forms, functions
import datetime
import logging
import csv
import requests
from django.contrib.auth.decorators import login_required




logger = logging.getLogger(__name__)

# Create your views here.
########## ##### ##### ##### ##### #####  index view ###### ##### ##### ##### ##### ##### ##### ##### #####

class search(TemplateView):
    template_name="YelpGetter/search.html"

    def get(self, request):
        form = forms.SearchForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        MY_API_KEY = "1S4HSe-tv0jePN4ytkR-6NLHx9zCRAEv9r3EU0pYeT1QzXGayzoIvOU2SGLLCkM5e9Rbm9L9AS6HhbbPe4feLFuBWvW8t6DvDObmgYKoHuN_h9pMVlp_J9WmvgnbXHYx"
        form = forms.SearchForm(request.POST)

        if form.is_valid():
            location = form.cleaned_data['location'] #uzimanje podataka
            term = form.cleaned_data['term']
            functions.scrape2(term, location)

            return redirect('history')

        args = {'form': form, 'location': location, 'term': term}
        return render(request, self.template_name, args)
    
def deep_search_url(request, uid):
    template_name="YelpGetter/search.html"
    functions.urlscrape(uid)
    return redirect('history')

def deep_search_mail(request, uid):
    template_name="YelpGetter/search.html"
    functions.mailscrape(uid)
    return redirect('history')
########## ##### ##### ##### ##### #####  latest view ###### ##### ##### ##### ##### ##### ##### ##### #####


def index(request):
    template_name ='YelpGetter/index.html'
    return render(request, template_name)


def history(request):
    all_entries = Search.objects.all().reverse()
    args = {'Search': all_entries}
    return render(request, 'YelpGetter/history.html', args)

########## ##### ##### ##### ##### #####  csv downl  ###### ##### ##### ##### ##### ##### ##### ##### #####

def dwn_csv(request, uid):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="results.csv"'
    all_entries = Result.objects.filter(uid=uid)
    writer = csv.writer(response)
    writer.writerow(['uid', 'name', 'yurl', 'phone', 'categories',  'rating', 'review_count', 'phone', 'sulr', 'address', 'citie', 'state' 'email'])
    for entry in all_entries:
        writer.writerow([entry.uid, entry.name, entry.yurl, entry.phone, entry.categories,  entry.rating, entry.review_count, entry.phone, entry.surl, entry.address, entry.city, entry.state, entry.mail])
    return response


def master(request):
    result_list = Result.objects.filter(uid='UPLOAD')

    args = { 'results': result_list, "total": len(result_list) }
    return render(request, 'YelpGetter/master.html',args)


########## ##### ##### ##### ##### #####  upload sheets  ###### ##### ##### ##### ##### ##### ##### ##### #####

def table(request, uid):
    all_entries = Result.objects.filter(uid=uid)
    searc_term = Search.objects.filter(uid=uid)
    args = {'Results': all_entries, 'total':len(all_entries), 'searc_term': searc_term[0] }
    return render(request, 'YelpGetter/table.html', args)


def tableterm(request, term):
    all_entries = Result.objects.filter(term=term)
    args = {'Results': all_entries, 'total':len(all_entries),'searc_term': term }
    return render(request, 'YelpGetter/tableterm.html', args)




def business_page(request, yurl):
    all_entries = Result.objects.filter(yurl=yurl)
    args = {'Results': all_entries, 
            'searc_term': term }
    return render(request, 'YelpGetter/business_page.html', args)




def tablestate(request, state):
    all_entries = []
    suma = Result.objects.filter(state=state)
    for s in suma:
        if s.uid != 'UPLOAD':
            all_entries.append(s)
    args = {'Results': all_entries, 'total':len(all_entries), 'state' :state }
    return render(request, 'YelpGetter/tablestate.html', args)


def term(request):
    all_entries = Search.objects.all()
    termslist = []
    for entry in all_entries:
        if entry.term.lower() not in termslist:
            termslist.append(entry.term.lower())
    args = {'termslist': termslist}
    return render(request, 'YelpGetter/term.html', args)


def cities(request):
    all_entries = State.objects.all()
    args = {'all_entries': all_entries}
    return render(request, 'YelpGetter/cities.html', args)


def city(request, state, city):
    all_entries = []
    suma = Result.objects.filter(city=city, state=state)
    for s in suma:
        if s.uid != 'UPLOAD':
            all_entries.append(s)

    args = {'all_entries': all_entries, 'city':city, 'state':state, 'total': len(all_entries)}
    return render(request, 'YelpGetter/tablecity.html', args)


def state(request):
    all_entries = Result.objects.all()
    statelist = []
    for entry in all_entries:
        if entry.state not in statelist:
            statelist.append(entry.state)
    args = {'statelist': statelist}
    return render(request, 'YelpGetter/state.html', args)

########## ##### ##### ##### ##### #####  status ###### ##### ##### ##### ##### ##### ##### ##### #####

def status(request):
    params = {'term': 'a', 'location': 'a'}
    url='https://api.yelp.com/v3/businesses/search'
    MY_API_KEY = "1S4HSe-tv0jePN4ytkR-6NLHx9zCRAEv9r3EU0pYeT1QzXGayzoIvOU2SGLLCkM5e9Rbm9L9AS6HhbbPe4feLFuBWvW8t6DvDObmgYKoHuN_h9pMVlp_J9WmvgnbXHYx"
    headers = {'Authorization': 'Bearer %s' % MY_API_KEY}
    params = {'term': 'chicago', 'location': 'caterer', }
    req = requests.get(url, headers=headers, params=params)
    item = ApiData(ResetTime=str(req.headers['RateLimit-ResetTime']), Remaining=str(req.headers['RateLimit-Remaining']))
    item.save()
    yelp = ApiData.objects.last()
    args = {'yelp':  yelp}
    return render(request, 'YelpGetter/status.html', args)


########## ##### ##### ##### ##### #####  upload csv ###### ##### ##### ##### ##### ##### ##### ##### #####

def upload_csv(request):
    if "GET" == request.method:
        return render(request, "status")

    try:
        csv_file = request.FILES["csv_file"]
        email=price=address = 0
        if 'email' in request.GET:
            email = 1
        if 'price' in request.GET:    
            price = 1
        if 'address' in request.GET:
            address = 1

        if not csv_file.name.endswith('.csv'):
            print('File is not CSV type')
            return HttpResponseRedirect("status")

        if csv_file.multiple_chunks():
            print("Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return HttpResponseRedirect("status")

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")

        for line in lines:
            try:
                fields = line.split(",")
                data = functions.getSheetData(fields)
                try:
                        resultDB = Result(uid='UPLOAD',
                                          name = data['name'],
                                          yurl =data['yurl'] ,
                                          review_count = '',
                                          address =data['address'] ,
                                          city =  data['city'],
                                          state = data['state'],
                                          categories = '',
                                          phone=data['phone'],
                                          surl = data['surl'],
                                          rating = ''
                                         )
                        resultDB.save()
                except:
                    pass
            except:
                pass

        searchDB = Search(
                                term = 'various',
                                location = 'various',
                                date = 'various',
                                uid = 'UPLOAD',
                            )
        searchDB.save()


    except Exception as e:
        print("Unable to upload file. "+repr(e))

    return HttpResponseRedirect("status")



def check_status(request):
    status = Status.objects.get(pk=1)
    status_arg = {
                    "yelp_scraper": status.yelp_scraper,
                    "url_scraper": status.url_scraper,
                    "email_scraper": status.email_scraper
                    }
    args = {
        'status': status_arg
    }
   
    return JsonResponse(args)

##master
#temporary fix to upload,  encoding problems, empty last row. need to find better solution for encoding









