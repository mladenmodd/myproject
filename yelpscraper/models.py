#!/usr/bin/env python

"""models.py: 2 models one that saves searaces second that saves results"""
"""last commit: 08012019 02:08 am"""

from django.db import models
import logging



class Status(models.Model):
    name = models.CharField(max_length=250, default='status')
    yelp_scraper = models.BooleanField(default=False)
    url_scraper = models.BooleanField(default=False)
    email_scraper = models.BooleanField(default=False)


class Search(models.Model):
    term = models.CharField(max_length=250)
    location = models.CharField(max_length=250)
    date = models.CharField(max_length=250)
    uid = models.CharField(max_length=250)


class Result(models.Model):
    uid = models.CharField(max_length=250, default='')
    name = models.CharField(max_length=250, default='')
    yurl = models.CharField(max_length=250, default='')
    phone = models.CharField(max_length=250, default='')
    categories = models.CharField(max_length=250, default='')
    rating = models.CharField(max_length=250, default='')
    review_count = models.CharField(max_length=250, default='')
    surl =  models.CharField(max_length=250, default='')
    address = models.CharField(max_length=250, default='')
    city = models.CharField(max_length=250, default='')
    state = models.CharField(max_length=250, default='')
    is_claimed = models.CharField(max_length=250, default='')
    term = models.CharField(max_length=250, default='')
    mail = models.EmailField(default='')
    

class ApiData(models.Model):
    ResetTime = models.CharField(max_length=250)
    Remaining = models.CharField(max_length=250)

class State(models.Model):
    city = models.CharField(max_length=250, default='')
    state = models.CharField(max_length=250, default='')


#todo: third model, that saves logins, users

